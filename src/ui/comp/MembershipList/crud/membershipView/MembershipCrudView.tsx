import React from 'react';
import autobind from 'autobind-decorator';
import { observer } from 'mobx-react';
import { Grid, TextField, Button } from '@material-ui/core';
import { Save, Update, Delete } from '@material-ui/icons';
import { AddButton, TrashButton } from '../../../../resource/ButtonColor';

interface Props{
//   addMembership : Function,
}
interface State{
}
@autobind
@observer
class MembershipCrudView extends React.Component<Props,State> {
  //
  state : State ={
  }

  render() {
    // const {addMembership} = this.props;

    return (
      <>
        <form>
          <Grid container spacing={1}>
            <Grid item xs={4}>
              <AddButton
                variant="contained"
                color="primary"
                startIcon={<Save />}
                // onClick={() => {addMembership}}
              >
                AddMembership
              </AddButton>
              &nbsp;&nbsp;
            </Grid>
          </Grid>
        </form>
      </>
    );
  }
}
export default MembershipCrudView;
