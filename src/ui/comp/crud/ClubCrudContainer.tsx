
import React from 'react';
import autobind from 'autobind-decorator';
import { inject, observer } from 'mobx-react';
import { ClubService, ClubsService } from '../../../service';
import ClubCrudView from './view/ClubCrudView';
import { TravelClub } from '../../../model';
import { RouteComponentProps, withRouter } from 'react-router-dom';
interface Props extends RouteComponentProps{
  //
  clubService?: ClubService;
  flagSwitch : Function;
}
@inject(ClubService.instanceName)
@autobind
@observer
class ClubCrudContainer extends React.Component<Props> {
  //

  lengthCondition(name : string, intro : string){
    if(name.length < 3){
      window.alert('이름은 3자 이상');
      return;
    }else if(intro.length < 10){
      window.alert('소개는 10자 이상');
      return;
    }
  }

  async setClub(name : string, intro : string){
    
    this.lengthCondition(name,intro);

    this.props.clubService?.setClub(new TravelClub(name, intro));
    await this.props.clubService?.registerClub(this.props.clubService.club!);
    window.alert('클럽이 추가되었습니다~');
    this.props.flagSwitch();
    // this.props.history.go(0);
  }

  // update 함수 짜는 중

  
  // updateClub(name : string, intro : string){
  
  //   this.lengthCondition(name,intro);
    
  //   let club = this.props.clubService?.findClubById(name);

  //   if(club == null){
  //     window.alert('클럽이 존재하지 않습니다.');
  //   }else{
  //     clubToUpdate = this.props.clubService?.setClub(club);
  //     this.props.clubService?.modifyClub(club);
  //   }
    
  // }
  
  render() {
    //
     return (
      <ClubCrudView
      setClub={this.setClub}
      />
    );
  }
}

export default withRouter(ClubCrudContainer);
